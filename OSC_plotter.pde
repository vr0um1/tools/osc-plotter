import oscP5.*;
import netP5.*;
import java.util.ArrayList;
import java.util.Collections;
import controlP5.*;

OscP5 oscP5;
ArrayList<ArrayList<Float>> oscValues;
int maxValues = 10000;  // Maximum number of values to display ~10min of data
float minValue, maxValue;
ControlP5 cp5;
CheckBox[] checkboxes;
boolean[] selectedData;
int graphRefreshRate = 10; // Refresh rate in milliseconds
boolean getData = false;
float zoomLevel = 1.0;  // Initial zoom level
float panX = 0.0;
float panY = 0.0;

int[] graphColors = {
  color(0, 0, 255), // Blue
  color(255, 0, 0), // Red
  color(0, 255, 0), // Green
  color(255, 255, 0), // Yellow
  color(0, 255, 255), // Cyan
  color(255, 0, 255), // Magenta
  color(255, 128, 0), // Orange
  color(128, 0, 255), // Purple
  color(255, 128, 128), // Light Red
  color(128, 255, 128), // Light Green
  color(128, 128, 255), // Light Blue
  color(255, 255, 128), // Light Yellow
  color(128, 255, 255), // Light Cyan
  color(255, 128, 255), // Light Magenta
  color(255, 128, 0), // Dark Orange
  color(128, 0, 128)    // Dark Purple
};

void setup() {
  size(1000, 400);
  // Add these declarations at the top of your code

  oscP5 = new OscP5(this, 12000);  // Set the port number to 12000 for OSC communication

  oscValues = new ArrayList<ArrayList<Float>>();

  minValue = Float.MAX_VALUE;
  maxValue = Float.MIN_VALUE;

  cp5 = new ControlP5(this);

  // Create checkboxes
  String[] oscAddresses = {"/pos_1", "/pos_2", "/pos_3", "/pos_4", "/pos_5", "/pos_6", "/pos_7", "/pos_8", "/pos_9", "/pos_10", "/pos_11", "/pos_12", "/pos_13", "/pos_14", "/pos_15"}; // Add more OSC addresses if needed
  checkboxes = new CheckBox[oscAddresses.length];
  selectedData = new boolean[oscAddresses.length];

  int checkboxHeight = 15;
  int checkboxMargin = 10;
  int startY = 10;
  int startX = width - 75;

  for (int i = 0; i < oscAddresses.length; i++) {
    checkboxes[i] = cp5.addCheckBox("checkbox" + i)
      .setPosition(startX, startY + i * (checkboxHeight + checkboxMargin))
      .setSize(20, 20)
      .setColorForeground(color(120))
      .setColorActive(color(255))
      .setColorLabel(color(0))
      .setItemsPerRow(1)
      .setSpacingRow(10)
      .addItem(oscAddresses[i], i);
    selectedData[i] = true;
  }

  // Add buttons
  int buttonWidth = 80;
  int buttonHeight = 30;
  int smallbuttonWidth = 40;
  int smallbuttonHeight = 30;
  cp5.addButton("resetButton")
    .setPosition(60, height - 40)
    .setSize(buttonWidth, buttonHeight)
    .setLabel("Reset Graph")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      resetGraph();
    }
  }
  );

  cp5.addButton("exportButton")
    .setPosition(160, height - 40)
    .setSize(buttonWidth, buttonHeight)
    .setLabel("Export")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      exportButtonPressed();
    }
  }
  );
  cp5.addButton("startButton")
    .setPosition(260, height - 40)
    .setSize(buttonWidth, buttonHeight)
    .setLabel("Start")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      startButtonPressed();
    }
  }
  );
  cp5.addButton("stopButton")
    .setPosition(360, height - 40)
    .setSize(buttonWidth, buttonHeight)
    .setLabel("Stop")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      stopButtonPressed();
    }
  }
  );
  cp5.addButton("zoom_plus")
    .setPosition(460, height - 40)
    .setSize(buttonWidth, buttonHeight)
    .setLabel("Zoom +")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      zoom_plusButtonPressed();
    }
  }
  );
  cp5.addButton("zoom_minus")
    .setPosition(560, height - 40)
    .setSize(buttonWidth, buttonHeight)
    .setLabel("Zoom -")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      zoom_minusButtonPressed();
    }
  }
  );
  cp5.addButton("panX_minus")
    .setPosition(660, height - 40)
    .setSize(smallbuttonWidth, smallbuttonHeight)
    .setLabel("<--")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      panX_minusButtonPressed();
    }
  }
  );
  cp5.addButton("panX_plus")
    .setPosition(710, height - 40)
    .setSize(smallbuttonWidth, smallbuttonHeight)
    .setLabel("-->")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      panX_plusButtonPressed();
    }
  }
  );
  cp5.addButton("panY_minus")
    .setPosition(760, height - 40)
    .setSize(smallbuttonWidth, smallbuttonHeight)
    .setLabel("Up")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      panY_minusButtonPressed();
    }
  }
  );
  cp5.addButton("panY_plus")
    .setPosition(810, height - 40)
    .setSize(smallbuttonWidth, smallbuttonHeight)
    .setLabel("Down")
    .addListener(new ControlListener() {
    public void controlEvent(ControlEvent event) {
      panY_plusButtonPressed();
    }
  }
  );
}

void draw() {
  background(255);

  // Draw the graph
  int offsetX = 50;
  int offsetY = 50;
  int graphWidth = width - 2 * offsetX;
  int graphHeight = height - 2 * offsetY;

  // Draw the y-axis
  stroke(0);
  line(offsetX, offsetY, offsetX, offsetY + graphHeight);

  // Draw the x-axis
  line(offsetX, offsetY + graphHeight, offsetX + graphWidth, offsetY + graphHeight);

  // Draw the graph lines
  for (int i = 0; i < oscValues.size(); i++) {
    ArrayList<Float> values = oscValues.get(i);
    if (values.size() > 1 ) {
      float xInterval = (float)graphWidth / (values.size() - 1);
      float yRange = (maxValue - minValue);
      float yScale = graphHeight / yRange;
      if (selectedData[i]) {
        stroke(graphColors[i % graphColors.length]);
      } else {
        stroke(255);
      }
      for (int j = 1; j < values.size(); j++) {
        float x1 = zoomLevel * (offsetX + (j - 1) * xInterval + panX);
        float y1 = zoomLevel * (offsetY + graphHeight - (values.get(j - 1) - minValue) * yScale + panY);
        float x2 = zoomLevel * (offsetX + j * xInterval+ panX);
        float y2 = zoomLevel * (offsetY + graphHeight - (values.get(j) - minValue) * yScale + panY);

        line(x1, y1, x2, y2);
      }
    }
  }

  // Draw the y-axis labels
  textAlign(RIGHT, CENTER);
  textSize(10);
  fill(0);
  float labelMargin = 5;
  float yStep = graphHeight / 10.0;
  for (int i = 0; i <= 10; i++) {
    float value = zoomLevel * (minValue + (maxValue - minValue)* i / 10.0);
    float y =zoomLevel * (panY + offsetY + graphHeight - i * yStep);
    text(nfc(value, 1), offsetX - labelMargin, y);
  }

  // Draw the legend
  int legendX = 10;
  int legendY = 10;
  int legendWidth = 120;
  int legendHeight = oscValues.size() * 15 + 10;
  fill(255);
  stroke(0);
  rect(legendX, legendY, legendWidth, legendHeight);
  fill(0);
  textAlign(LEFT, CENTER);
  textSize(10);
  for (int i = 0; i < oscValues.size(); i++) {
    String oscAddress = checkboxes[i].getItem(0).getLabel();
    fill(graphColors[i % graphColors.length]);
    text(oscAddress, legendX + 5, legendY + 5 + i * 15);
  }
}

void resetGraph() {
  oscValues.clear();
  minValue = Float.MAX_VALUE;
  maxValue = Float.MIN_VALUE;
}

void keyPressed() {
  if (key == ' ') {
    resetGraph();
  }
}

void oscEvent(OscMessage theOscMessage) {
  // Check if the OSC address exists in the checkboxes and if it is selected
  int oscIndex = -1;
  if (getData == true) {
    for (int i = 0; i < checkboxes.length; i++) {
      //if (checkboxes[i].getState(0)) {
      String oscAddress = checkboxes[i].getItem(0).getLabel();
      if (theOscMessage.checkAddrPattern(oscAddress)) {
        oscIndex = i;
        break;
        //}
      }
    }

    if (oscIndex != -1) {
      float oscValue = float(theOscMessage.get(0).intValue());
      ArrayList<Float> values;
      if (oscValues.size() <= oscIndex) {
        values = new ArrayList<Float>();
        oscValues.add(values);
      } else {
        values = oscValues.get(oscIndex);
      }

      values.add(oscValue);
      if (values.size() > maxValues) {
        values.remove(0);
      }

      if (oscValue < minValue) {
        minValue = oscValue;
      }
      if (oscValue > maxValue) {
        maxValue = oscValue;
      }
    }
  }
}

void addValueToGraph(int index, float value) {
  ArrayList<Float> values;
  if (oscValues.size() <= index) {
    values = new ArrayList<Float>();
    oscValues.add(values);
  } else {
    values = oscValues.get(index);
  }

  values.add(value);
  if (values.size() > maxValues) {
    values.remove(0);
  }

  // Update min and max values
  updateMinMaxValues();
}

void updateMinMaxValues() {
  minValue = Float.MAX_VALUE;
  maxValue = Float.MIN_VALUE;

  for (ArrayList<Float> values : oscValues) {
    for (float value : values) {
      if (value < minValue)
        minValue = value;
      if (value > maxValue)
        maxValue = value;
    }
  }
}

void drawGrid() {
  // Draw vertical grid lines
  int numLinesX = 10;
  float xSpacing = (width - 120) / (float) numLinesX;
  stroke(200);
  strokeWeight(0.5);
  for (int i = 0; i <= numLinesX; i++) {
    float x = 60 + i * xSpacing;
    line(x, 60, x, height - 60);
  }

  // Draw horizontal grid lines
  int numLinesY = 8;
  float ySpacing = (height - 120) / (float) numLinesY;
  for (int i = 0; i <= numLinesY; i++) {
    float y = height - 60 - i * ySpacing;
    line(60, y, width - 60, y);
  }
}

void drawLabelsAndTitles() {
  // Draw x-axis label
  fill(0);
  textAlign(CENTER, CENTER);
  textSize(12);
  text("Time", width / 2, height - 30);

  // Draw y-axis label
  pushMatrix();
  translate(30, height / 2);
  rotate(-HALF_PI);
  text("Value", 0, 0);
  popMatrix();

  // Draw graph title
  textSize(18);
  text("OSC Plotter", width / 2, 30);
}

void resetButtonPressed() {
  oscValues.clear();
  minValue = Float.MAX_VALUE;
  maxValue = Float.MIN_VALUE;
  zoomLevel = 1.0;
  panX= 0;
  panY = 0;
}

import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;

void exportButtonPressed() {
  if (oscValues.size() > 0) {
    String absolutePath = sketchPath("osc_data.csv");

    try {
      FileWriter writer = new FileWriter(absolutePath, true);

      int maxDataSize = Integer.MAX_VALUE;
      for (int j = 0; j < oscValues.size(); j++) {
        //if (selectedData[j]) {
        maxDataSize = Math.min(maxDataSize, oscValues.get(j).size());
        //}
      }

      for (int i = 0; i < maxDataSize; i++) {
        StringBuilder line = new StringBuilder();
        boolean isFirstValueAdded = false;
        for (int j = 0; j < oscValues.size(); j++) {
          //if (selectedData[j]) {
          if (isFirstValueAdded) {
            line.append(",");
          }
          line.append(oscValues.get(j).get(i));
          isFirstValueAdded = true;
          //}
        }
        writer.write(line.toString());
        writer.write("\n");
      }

      writer.close();
      println("Data exported to " + absolutePath);
    }
    catch (IOException e) {
      println("Error exporting data: " + e.getMessage());
    }
  }
}

void startButtonPressed() {
  if (getData == false) {
    resetButtonPressed();
  }
  getData = true;
}
void stopButtonPressed() {
  getData = false;
}

void zoom_plusButtonPressed() {
  zoomLevel *= 2;
}

void zoom_minusButtonPressed() {
  zoomLevel /= 2;
}
void panX_plusButtonPressed() {
  panX -= 25;
}

void panX_minusButtonPressed() {
  panX += 25;
}
void panY_plusButtonPressed() {
  panY -= 10;
}

void panY_minusButtonPressed() {
  panY += 10;
}

void controlEvent(ControlEvent event) {
  if (event.isGroup()) {
    for (int i = 0; i < checkboxes.length; i++) {
      if (event.getGroup().getName().equals("checkbox" + i)) {
        selectedData[i] = event.getGroup().getArrayValue(0) < 1;
      }
    }
  }
}


// Run the sketch
public static void main(String[] args) {
  PApplet.main("OSC_plotter");
}
